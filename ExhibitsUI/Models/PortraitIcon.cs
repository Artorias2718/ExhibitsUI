﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExhibitsUI.Models
{
    [JsonObject]
    public class PortraitIcon
    {
        [JsonProperty("id")]
        public int id { get; set; }
        [JsonProperty("name")]
        public string name { get; set; } 
        [JsonProperty("location")]
        public Dictionary<string, int> location { get; set; }
    }
}
