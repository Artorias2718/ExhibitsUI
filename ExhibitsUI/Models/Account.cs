﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExhibitsUI.Models
{
    [JsonObject]
    class Account
    {
        [JsonProperty ("username")]
        public string user { get; set; }
        [JsonProperty ("password")]
        public string pass { get; set; }
        [JsonProperty("salt")]
        public string salt { get; set; }
    }
}
