﻿//using Boomlagoon.JSON;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;
using Newtonsoft.Json.Serialization;
using Newtonsoft.Json;
using ExhibitsUI.Models;

namespace ExhibitsUI.Forms
{
    public enum AccessMode
    {
        ADMIN,
        STANDARD
    }

    public enum EditMode
    {
        NONE,
        IMAGES
    }

    public partial class Master : Form
    {
        private static Master form;
        private string directory;
        private UI uiForm;
        private Point mouseCoords;
        private List<string> fNames;
        private bool isBackground = false;
        public AdminMenu menu = new AdminMenu();
        private string background;

        private const int ICON_WIDTH = 150;
        private const int ICON_HEIGHT = 150;

        public AccessMode access;
        public EditMode edit;

        public List<string> iconNames;
        public List<string> detailNames;

        private List<PortraitIcon> portraits;
        List<DetailIcon> details;

        public string BackgroundName
        {
            get
            {
                return background;
            }
            set
            {
                background = value;
            }
        }

        
        public List<PortraitIcon> PortraitList
        {
            get
            {
                return portraits;
            }
            set
            {
                portraits = value;
            }
        }

        private Master()
        {
            form = this;
            uiForm = new UI();
            directory = "";
            mouseCoords = new Point();

            fNames = new List<string>();
            iconNames = new List<string>();
            detailNames = new List<string>();

            portraits = new List<PortraitIcon>();
            details = new List<DetailIcon>();
        }

        public static Master Instance {
            get
            {
                if (form == null)
                {
                    form = new Master();
                }
                return form;
            }
            set
            {
                if(form == null)
                {
                    form = new Master();
                }
                else
                {
                    return;
                }
            }
        }

        public void AddFile(string path, string type)
        {
            string filePath = Environment.CurrentDirectory; 
            string imageRoot = filePath + "\\Assets\\Img";

            if(type == "UI")
            {
                Image img = Image.FromFile(directory + "\\" + path);
                ImageFormat format = img.RawFormat;

                isBackground = new Regex("^.*(?(Background)|(Bkg]))+.*$", RegexOptions.IgnoreCase).IsMatch(path);

                if (!File.Exists(imageRoot + "\\" + path))
                {
                    img.Save(imageRoot + "\\" + type + "\\" + path, format);
                    UpdateSaveFile(path);
                }
                else
                {
                    UpdateSaveFile(path);
                }
            }
            else if(type == "Bio" && !File.Exists(imageRoot + "\\" + type + "\\" + path))
            {
                File.Copy(path, imageRoot + type + path);
            } 
        }

        void UpdateSaveFile(string path)
        {
            BackColor = Color.FromName("Control");

            bool fileEmpty = false;

            string filePath = Environment.CurrentDirectory + "\\Assets\\Text\\UI.json";
            StreamReader reader = File.OpenText(filePath);

            string contents = reader.ReadToEnd();

            reader.Close();

            StreamWriter writer = new StreamWriter(filePath, false);

            if (contents.Length > 0)
            {
                fileEmpty = false;
            }
            else
            {
                fileEmpty = true;
            }

            int x = mouseCoords.X;
            int y = mouseCoords.Y;

            int i = 0;

            PortraitIcon newPortrait = new PortraitIcon();
            PortraitIcon backIcon = new PortraitIcon();

            if (!fileEmpty)
            {
                portraits = JsonConvert.DeserializeObject<List<PortraitIcon>>(contents);
            }
            else
            {
                portraits = new List<PortraitIcon>();
            }

            if (iconNames.IndexOf(path) > 0)
            {
                i = iconNames.IndexOf(path);
            }
            else
            {
                i = portraits.Count;
            }

                isBackground = new Regex("^.*(?(Background)|(Bkg]))+.*$", RegexOptions.IgnoreCase).IsMatch(path);

                if (!isBackground)
                {
                    newPortrait.id = i;
                    newPortrait.location = new Dictionary<string, int>();

                    newPortrait.location.Add("xPos", x);
                    newPortrait.location.Add("yPos", y);
                    newPortrait.name = path;


                    if (portraits.Find(p => p.id == i) != null)
                    {
                        PortraitIcon tempIcon = portraits.Find(p => p.id == i);
                        tempIcon.location["xPos"] = x;
                        tempIcon.location["yPos"] = y;
                        i++;
                    }
                    else
                    {
                        portraits.Add(newPortrait);
                        i++;
                    }
                }

                else if (isBackground)
                { 
                    backIcon.id = Master.Instance.iconNames.Count;
                    backIcon.location = new Dictionary<string, int>();
                    backIcon.location.Add("xPos", 0);
                    backIcon.location.Add("yPos", 0);
                    backIcon.name = path;

                    i = iconNames.Count;
                    x = Instance.GetCoords.X;
                    y = Instance.GetCoords.Y;

                    portraits.Add(backIcon);
                    i++;
                }

                List<PortraitIcon> tempList = new List<PortraitIcon>();

                if(portraits.Count > 0)
                {
                    foreach (PortraitIcon p in portraits)
                    {
                        if (p.id == backIcon.id)
                        {
                            continue;
                        }
                        else
                        {
                            tempList.Add(p);
                        }
                    }
                }

                tempList.Add(portraits.Find(port => port.id == backIcon.id)); 

                for(int j = 0; j < tempList.Count; j++)
                {
                    tempList[j].id = j;
                }

                portraits = tempList;

                string json = JsonConvert.SerializeObject(portraits, Formatting.Indented);

                writer.Write(json);
                writer.Close();
        }
        public Image FixedSize(Image image, int Width, int Height, bool needToFill)
        {
            #region много арифметики
            int sourceWidth = image.Width;
            int sourceHeight = image.Height;
            int sourceX = 0;
            int sourceY = 0;
            double destX = 0;
            double destY = 0;

            double nScale = 0;
            double nScaleW = 0;
            double nScaleH = 0;

            nScaleW = ((double)Width / (double)sourceWidth);
            nScaleH = ((double)Height / (double)sourceHeight);
            if (!needToFill)
            {
                nScale = Math.Min(nScaleH, nScaleW);
            }
            else
            {
                nScale = Math.Max(nScaleH, nScaleW);
                destY = (Height - sourceHeight * nScale) / 2;
                destX = (Width - sourceWidth * nScale) / 2;
            }

            nScale = Math.Min(1.0, nScale);

            int destWidth = (int)Math.Round(sourceWidth * nScale);
            int destHeight = (int)Math.Round(sourceHeight * nScale);
            #endregion

            System.Drawing.Bitmap bmPhoto = null;
            try
            {
                bmPhoto = new System.Drawing.Bitmap(destWidth + (int)Math.Round(2 * destX), destHeight + (int)Math.Round(2 * destY));
            }
            catch (Exception ex)
            {
                throw new ApplicationException(string.Format("destWidth:{0}, destX:{1}, destHeight:{2}, desxtY:{3}, Width:{4}, Height:{5}",
                    destWidth, destX, destHeight, destY, Width, Height), ex);
            }
            using (System.Drawing.Graphics grPhoto = System.Drawing.Graphics.FromImage(bmPhoto))
            {
                grPhoto.InterpolationMode = InterpolationMode.HighQualityBicubic;
                grPhoto.CompositingQuality = CompositingQuality.HighQuality;
                grPhoto.SmoothingMode = SmoothingMode.HighQuality;

                Rectangle to = new System.Drawing.Rectangle((int)Math.Round(destX), (int)Math.Round(destY), destWidth, destHeight);
                Rectangle from = new System.Drawing.Rectangle(sourceX, sourceY, sourceWidth, sourceHeight);
                grPhoto.DrawImage(image, to, from, System.Drawing.GraphicsUnit.Pixel);
                return bmPhoto;
            }
        }

        public void Shutdown(object sender, EventArgs e)
        {
            Application.Exit();
        }

        public string GetSelectedDirectory
        {
            get
            {
                return directory;
            }
            set
            {
                directory = value;
            }
        }

        public UI GetUI
        {
            get
            {
                return uiForm;
            }
            set
            {
                uiForm = value;
            }
        }

        public Point GetCoords
        {
            get
            {
                return mouseCoords;
            }
            set
            {
                mouseCoords = value;
            }
        }

        public List<string> GetNames
        {
            get
            {
                return fNames;
            }
            set
            {
                fNames = value;
            }
        }
    }
}
