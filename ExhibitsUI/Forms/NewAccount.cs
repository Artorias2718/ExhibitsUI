﻿using ExhibitsUI.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ExhibitsUI.Forms
{
    public partial class NewAccount: Form
    {
        private string salt;

        public NewAccount()
        {
            InitializeComponent();
        }

        private void ShowUI(object sender, EventArgs e)
        {
            Master.Instance.GetUI.Show();
            this.Hide();
        }

        // Random salt value

        private string GenerateSalt(int size)
        {
            RNGCryptoServiceProvider rng = new RNGCryptoServiceProvider();
            byte[] buff = new byte[size];
            rng.GetBytes(buff);

            return Convert.ToBase64String(buff);
        }

        // SHA-256

        private string GenerateHash(string input, string salt)
        {
            byte[] bytes = Encoding.UTF8.GetBytes(input + salt);
            SHA256Managed hashObj = new SHA256Managed();

            byte[] hash = hashObj.ComputeHash(bytes);

            return GenerateHex(hash);
        }

        private string GenerateHex(byte[] data)
        {
            StringBuilder builder = new StringBuilder();

            data = DecimalToHex(data);
            
            for(int i = 0; i < data.Length; i++)
            {
                switch (data[i])
                {
                    case 10:
                        builder.Append('A');
                        break;
                    case 11:
                        builder.Append('B');
                        break;
                    case 12:
                        builder.Append('C');
                        break;
                    case 13:
                        builder.Append('D');
                        break;
                    case 14:
                        builder.Append('E');
                        break;
                    case 15:
                        builder.Append('F');
                        break;
                    default:
                        builder.Append(data[i]);
                        break;
                }
            }

            string result = builder.ToString();
            return result;
        }

        private byte[] DecimalToHex(byte[] data)
        {
            int result = 0;
            for(int i = 0; i < data.Length; i++)
            {
                result = data[i];
                while(result != 0)
                {
                    result /= 16;
                    data[i] = (byte)(data[i] % 16);
                }
            }

            return data;
        }

        private void CreateAccount(object sender, EventArgs e)
        {
            if(((MouseEventArgs)e).Button == MouseButtons.Left)
            {
                SaveAccount();
            } 
        }

        private void SaveAccount()
        {
            string filePath = Environment.CurrentDirectory;
            filePath = filePath + "\\Assets\\text\\Login.json";
            StreamReader reader = File.OpenText(filePath);

            string contents = reader.ReadToEnd();

            reader.Close();
            
            if(contents.Length == 0)
            {
                if(PassTxt.Text == ConfirmTxt.Text)
                {
                    StreamWriter writer = new StreamWriter(filePath, false);
                    Account account = new Account();

                    salt = GenerateSalt(10);

                    account.user = UserTxt.Text;
                    account.salt = salt;
                    account.pass = GenerateHash(PassTxt.Text, account.salt); 

                    writer.Write(JsonConvert.SerializeObject(account, Formatting.Indented));
                    writer.Close();
                    this.FindForm().Close();
                }
                else
                {
                    MessageBox.Show("Passwords do not match!");
                }
            }
        }
    }
}
