﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows.Forms;

namespace ExhibitsUI.Forms
{
    public partial class AdminMenu : Form
    {
        public AdminMenu()
        { 
            InitializeComponent();
        }

        private void ClickButton(object sender, EventArgs e)
        {
            if(((MouseEventArgs)e).Button == MouseButtons.Left)
            {
                if (((Button)sender).Name == "EditImgBtn")
                {
                    Master.Instance.edit = EditMode.IMAGES;
                }
                else if (((Button)sender).Name == "StartBtn")
                {
                    Master.Instance.edit = EditMode.NONE;
                }
            }

            if(Master.Instance.edit == EditMode.IMAGES)
            {
                if (Master.Instance.GetUI.IsDisposed)
                {
                    Master.Instance.GetUI = new UI();
                }
                Master.Instance.GetUI.BackColor = Color.LightCyan;
                Master.Instance.GetUI.Show();
            }
            else if (Master.Instance.edit == EditMode.NONE)
            {
                if (Master.Instance.GetUI.IsDisposed)
                {
                    Master.Instance.GetUI = new UI();
                }
                Master.Instance.GetUI.BackColor = Color.FromName("Control");
                Master.Instance.GetUI.Show();
            }
        }

        private void Edit(object sender, EventArgs e)
        {
            EditAccount edit = new EditAccount();
            if(edit.IsDisposed)
            {
                edit = new EditAccount();
                edit.Show();
            }
            else
            {
                edit.Show();
            }
        }

        private void Shutdown(object sender, EventArgs e)
        {
            this.Close();
            Application.Exit();
        }
    }
}
