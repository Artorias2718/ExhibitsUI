﻿using ExhibitsUI.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ExhibitsUI.Forms
{
    public partial class Login : Form
    {
        public Login()
        {
            InitializeComponent();
        }

        private void ShowUI(object sender, EventArgs e)
        {
            Master.Instance.GetUI.Show();
            this.Hide();
        }

        private void LoginAccount(object sender, EventArgs e)
        {
            AdminLogin();
        }

        private void EditAccount(object sender, EventArgs e)
        {
            if (((MouseEventArgs)e).Button == MouseButtons.Left)
            {
                EditAccount edit = new EditAccount();
                edit.Show();
            }
        }

        private void AdminLogin()
        {
            string filePath = Environment.CurrentDirectory + "\\Assets\\text\\Login.json";
            StreamReader reader = File.OpenText(filePath);

            string contents = reader.ReadToEnd();

            Account account = JsonConvert.DeserializeObject<Account>(contents);

            string userPass = GenerateHash(PassTxt.Text, account.salt);
            string accountPass = account.pass;

            if (UserTxt.Text != account.user || accountPass != userPass)
            {
                MessageBox.Show("Incorrect Username or Password!");
            }
            else
            {
                Master.Instance.access = AccessMode.ADMIN; 
                Master.Instance.menu.Show();
                this.Hide(); 
            }
        }

        // SHA-256

        private string GenerateHash(string input, string salt)
        {
            byte[] bytes = Encoding.UTF8.GetBytes(input + salt);
            SHA256Managed hashObj = new SHA256Managed();

            byte[] hash = hashObj.ComputeHash(bytes);

            return GenerateHex(hash);
        }

        private string GenerateHex(byte[] data)
        {
            StringBuilder builder = new StringBuilder();

            data = DecimalToHex(data);
            
            for(int i = 0; i < data.Length; i++)
            {
                switch (data[i])
                {
                    case 10:
                        builder.Append('A');
                        break;
                    case 11:
                        builder.Append('B');
                        break;
                    case 12:
                        builder.Append('C');
                        break;
                    case 13:
                        builder.Append('D');
                        break;
                    case 14:
                        builder.Append('E');
                        break;
                    case 15:
                        builder.Append('F');
                        break;
                    default:
                        builder.Append(data[i]);
                        break;
                }
            }

            string result = builder.ToString();
            return result;
        }

        private byte[] DecimalToHex(byte[] data)
        {
            int result = 0;
            for(int i = 0; i < data.Length; i++)
            {
                result = data[i];
                while(result != 0)
                {
                    result /= 16;
                    data[i] = (byte)(data[i] % 16);
                }
            }

            return data;
        }

        private void CreateAccount(object sender, EventArgs e)
        {
            NewAccount newAcct = new NewAccount(); 

            if(newAcct.IsDisposed)
            {
                newAcct = new NewAccount();
                newAcct.Show();
            }
            else
            {
                newAcct.Show();
            } 
        }

        private void StartUI(object sender, EventArgs e)
        { 
            if(Master.Instance.GetUI.IsDisposed)
            {
                Master.Instance.GetUI = new UI();
                Master.Instance.access = AccessMode.STANDARD;
                Master.Instance.GetUI.Show();
            }
            else
            {
                Master.Instance.access = AccessMode.STANDARD;
                Master.Instance.GetUI.Show();
            }
        }

        private void Shutdown(object sender, EventArgs e)
        {
            this.Close();
            Application.Exit();
        }
    }
}
