﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ExhibitsUI.Forms
{
    public partial class Gallery : Form
    {
        private PictureBox selectedImage;
        private List<PictureBox> iconControls;

        private bool isBackground;
        private bool imgSelected;

        string fileName;

        public Gallery()
        {
            InitializeComponent();
            selectedImage = new PictureBox();
            iconControls = new List<PictureBox>();
            imgSelected = false;

            fileName = "";
        }

        public Panel GetImagePanel()
        {
            return ImagePanel;
        }

        public List<PictureBox> GetIconControls()
        {
            return iconControls;
        }

        public void clickImg(object sender, EventArgs e)
        {
            PictureBox pBox = (PictureBox) sender;
            if (iconControls.Contains(pBox))
            {
                if((!imgSelected) && ((MouseEventArgs)e).Clicks == 1 && (((MouseEventArgs)e).Button == MouseButtons.Left))
                {
                    imgSelected = true;
                }
                else if ((imgSelected) && ((MouseEventArgs)e).Clicks == 1 && (((MouseEventArgs)e).Button == MouseButtons.Left))
                {
                    imgSelected = false;
                }

                if (imgSelected)
                {
                    pBox.BackColor = Color.Red;
                }
                else
                {
                    pBox.BackColor = Color.FromName("Control");
                }
                selectedImage = pBox;
            }
        }

        private void Confirm(object sender, EventArgs e)
        {
            fileName = selectedImage.Name.Substring(selectedImage.Name.LastIndexOf("\\") + 1, selectedImage.Name.Length - selectedImage.Name.LastIndexOf("\\") - 1);
            selectedImage.Name = fileName.Substring(0, fileName.Length);

            isBackground = new Regex("^.*(?(Background)|(Bkg]))+.*$", RegexOptions.IgnoreCase).IsMatch(selectedImage.Name);

            Master.Instance.BackgroundName = selectedImage.Name.Substring(0, fileName.LastIndexOf('.'));
            selectedImage.BackColor = Color.FromName("Control");
           
            if((selectedImage.Name.Contains(".jpg")) || (selectedImage.Name.Contains(".png")))
            {
                Master.Instance.AddFile(selectedImage.Name, "UI");
            }
            else if((selectedImage.Name.Contains(".pdf")))
            {
                Master.Instance.AddFile(selectedImage.Name, "Bio");
            }

            Close();
        }

        private void Cancel(object sender, EventArgs e)
        {
            Close();
        }
    }
}
