﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ExhibitsUI.Models;
using System.Text.RegularExpressions;

namespace ExhibitsUI.Forms
{
    public partial class Details : Form
    {
        public Point imgLocation;

        public Details()
        {
            InitializeComponent();
            this.backBtn.Location = new Point(this.ClientSize.Width - backBtn.Size.Width - 5, 5);
        }
    
        public void ReadData(int imgID)
        {
            BackColor = Color.FromName("Control");

            string fullPath = Environment.CurrentDirectory;
            string imageRoot = fullPath + "\\Assets\\Img";

            List<PortraitIcon> iconList = Master.Instance.PortraitList;

            info.Name = iconList[imgID].name.Substring(0, iconList[imgID].name.LastIndexOf('.')) + ".pdf";

            string infoPath = imageRoot + "\\Bio\\" + info.Name;
            info.Navigate(infoPath);
        }

        private void ClickBack(object sender, EventArgs e)
        {
            Close();
        }
    }
}
