﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using ExhibitsUI.Models;

namespace ExhibitsUI.Forms
{
    public partial class UI : Form
    {
        private Gallery imageSelector;
        public List<Image> galleryList;
        public List<PictureBox> containers;
        public Details details;

        private bool isBackground;

        public UI()
        {
            InitializeComponent();
            containers = new List<PictureBox>();
            details = new Details();
            isBackground = false;
        }

        private void UIForm_Load(object sender, EventArgs e)
        {
            LoadMainUI();
            if (Master.Instance.edit == EditMode.IMAGES)
            {
                foreach (PictureBox pBox in containers)
                {
                    pBox.Enabled = false;
                }
                this.MouseClick += MouseClickEvent;
            }
            else if(Master.Instance.edit == EditMode.NONE)
            {
                foreach(PictureBox pBox in Controls)
                {
                    isBackground = new Regex("^.*(?(Background)|(Bkg]))+.*$", RegexOptions.IgnoreCase).IsMatch(pBox.Name);
                    if (!isBackground)
                    {           
                        pBox.Enabled = true;
                        pBox.Click += new EventHandler(ClickIcon);
                    }
                }
            } 
        }

        void LoadMainUI()
        {
            string fullPath = Environment.CurrentDirectory;
            string saveFilePath = fullPath + "\\Assets\\Text\\UI.json";
            string imageRoot = fullPath + "\\Assets\\Img\\UI\\";

            StreamReader reader = File.OpenText(saveFilePath);

            string contents = reader.ReadToEnd();

            reader.Close();

            Master.Instance.PortraitList = new List<PortraitIcon>();

            if(!string.IsNullOrEmpty(contents))
            {
                Master.Instance.PortraitList = JsonConvert.DeserializeObject<List<PortraitIcon>>(contents);

                foreach (PortraitIcon icon in Master.Instance.PortraitList)
                {
                    PictureBox pBox = new PictureBox();
                    isBackground = isBackground = new Regex("^.*(?(Background)|(Bkg]))+.*$", RegexOptions.IgnoreCase).IsMatch(icon.name);
                    if (!isBackground)
                    {
                        pBox.Name = icon.name;
                        pBox.Image = Image.FromFile(imageRoot + icon.name).GetThumbnailImage(150, 150, null, IntPtr.Zero);
                        pBox.Image = Master.Instance.FixedSize(pBox.Image, 50, 50, true);

                        pBox.SizeMode = PictureBoxSizeMode.AutoSize;
                        pBox.Location = new Point(icon.location["xPos"], icon.location["yPos"]);
                        pBox.Padding = new Padding(5);
                        pBox.BackColor = Color.Black;
                        Controls.Add(pBox);
                        Master.Instance.iconNames.Add(pBox.Name);
                    }
                    else
                    {    
                        BackgroundImage = Image.FromFile(imageRoot + icon.name);
                        Master.Instance.iconNames.Add(icon.name);
                    }
                }
            } 
        }

        public void ClickIcon(object sender, EventArgs e)
        {
            if (((MouseEventArgs)e).Button == MouseButtons.Left)
            {
                PictureBox pBox = (PictureBox)sender;

                int id = Controls.IndexOf(pBox);
 
                pBox.BackColor = Color.Red;

                details.ReadData(id);
                details.ShowDialog(this);

                pBox.BackColor = Color.Black;
            }
        }

        public void GetMouseCoordinates()
        {
            Master.Instance.GetCoords = PointToClient(MousePosition);
            return;
        }

        public void DisplayGallery()
        {
            FolderBrowserDialog folderBrowser = new FolderBrowserDialog();

            DialogResult result = folderBrowser.ShowDialog();

            Master.Instance.GetSelectedDirectory = folderBrowser.SelectedPath;

            string fullPath = Environment.CurrentDirectory;
            string saveFilePath = fullPath + "\\Assets\\Text\\UI.json";
            string imageRoot = fullPath + "\\Assets\\Img";

            string fileName = "";

            if (!string.IsNullOrWhiteSpace(Master.Instance.GetSelectedDirectory))
            {
                string filters = ".*.(jpg|png|pdf)";
                string[] files = FindFiles(folderBrowser, filters);

                imageSelector = new Gallery();

                Point start = new Point();

                int offsetX = 0;
                int offsetY = 0;

                int imgCount = 0;
                int rowCount = 3;

                string bkg = "";


                foreach (string file in files)
                {
                    fileName = file.Substring(file.LastIndexOf("\\") + 1, file.Length - file.LastIndexOf("\\") - 1);
                    if(file.Contains(".jpg") || file.Contains(".png"))
                    {
                        imageRoot = fullPath + "\\Assets\\Img\\UI";
                        PictureBox pBox = new PictureBox();
                        pBox.Image = Image.FromFile(file).GetThumbnailImage(500, 500, null, IntPtr.Zero);
                        pBox.Image = Master.Instance.FixedSize(pBox.Image, 200, 200, true);

                        pBox.SizeMode = PictureBoxSizeMode.AutoSize;
                        pBox.Padding = new Padding(5);

                        isBackground = new Regex("^.*(?(Background)|(Bkg]))+.*$", RegexOptions.IgnoreCase).IsMatch(file);

                        if (!isBackground)
                        {
                            Master.Instance.GetNames.Add(file);
                        }
                        else
                        {
                            bkg = file;
                        }

                        start = new Point(offsetX, offsetY);
                        pBox.Location = start;

                        pBox.Name = file.Substring(file.LastIndexOf("\\") + 1, (file.Length - file.LastIndexOf("\\") - 1));

                        imageSelector.GetIconControls().Add(pBox);

                        if ((imgCount + 1) % rowCount == 0)
                        {
                            offsetX = 0;
                            offsetY = pBox.Location.Y + 225;
                        }
                        else
                        {
                            offsetX = pBox.Location.X + 225;
                        }
                        imgCount++;
                    }
                    else
                    {
                        imageRoot = fullPath + "\\Assets\\Img\\Bio";
                        if (!File.Exists(imageRoot + "\\" + fileName))
                        { 
                            File.Copy(file, imageRoot + "\\" + fileName, true);
                        }
                    }
                }

                if(!string.IsNullOrEmpty(bkg))
                {
                    Master.Instance.GetNames.Add(bkg);
                    imgCount++;
                }

                foreach (PictureBox pBox in imageSelector.GetIconControls())
                {
                    pBox.Click += new EventHandler(imageSelector.clickImg);
                    imageSelector.GetImagePanel().Controls.Add(pBox);
                }

                imageSelector.ShowDialog();
            }
            return;
        }

        private string[] FindFiles(FolderBrowserDialog dialog, string pattern)
        {
            Regex regex = new Regex(pattern);

            List<string> files = new List<string>();

            for (int i = 0; i < Directory.GetFiles(dialog.SelectedPath).Length; i++)
            {
                bool found = regex.IsMatch(Directory.GetFiles(dialog.SelectedPath)[i]);
                if (found)
                {
                    files.Add(Directory.GetFiles(dialog.SelectedPath)[i]);
                }
            }

            return files.ToArray();
        }

        private void MouseClickEvent(object sender, EventArgs e)
        {
            if(Master.Instance.edit == EditMode.IMAGES)
            {
                if (((MouseEventArgs)e).Button == MouseButtons.Left)
                {
                    GetMouseCoordinates();
                    DisplayGallery();
                }
            }
            else if(Master.Instance.edit == EditMode.NONE)
            {
                if (((MouseEventArgs)e).Button == MouseButtons.Left)
                {
                    PictureBox pBox = (PictureBox)sender;
                }
            }
        }

        private void Shutdown(object sender, EventArgs e)
        {
            this.Close();
            Master.Instance.menu.Focus();
        }
    }
}
