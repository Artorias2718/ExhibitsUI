﻿namespace ExhibitsUI.Forms
{
    partial class AdminMenu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.AccountBtn = new System.Windows.Forms.Button();
            this.EditImgBtn = new System.Windows.Forms.Button();
            this.PreviewBtn = new System.Windows.Forms.Button(); 
            this.SuspendLayout();
            // 
            // AccountBtn
            // 
            this.AccountBtn.Location = new System.Drawing.Point(76, 55);
            this.AccountBtn.Name = "AccountBtn";
            this.AccountBtn.Size = new System.Drawing.Size(125, 23);
            this.AccountBtn.TabIndex = 0;
            this.AccountBtn.Text = "Manage Account";
            this.AccountBtn.UseVisualStyleBackColor = true;
            this.AccountBtn.Click += Edit;
            // 
            // EditImgBtn
            // 
            this.EditImgBtn.Location = new System.Drawing.Point(75, 86);
            this.EditImgBtn.Name = "EditImgBtn";
            this.EditImgBtn.Size = new System.Drawing.Size(125, 23);
            this.EditImgBtn.TabIndex = 1;
            this.EditImgBtn.Text = "Manage Assets";
            this.EditImgBtn.UseVisualStyleBackColor = true;
            this.EditImgBtn.Click += new System.EventHandler(this.ClickButton);
            // 
            // PreviewBtn
            // 
            this.PreviewBtn.Location = new System.Drawing.Point(76, 116);
            this.PreviewBtn.Name = "PreviewBtn";
            this.PreviewBtn.Size = new System.Drawing.Size(125, 23);
            this.PreviewBtn.TabIndex = 2;
            this.PreviewBtn.Text = "Preview Interface";
            this.PreviewBtn.UseVisualStyleBackColor = true;
            this.PreviewBtn.Click += new System.EventHandler(this.ClickButton);
            // 
            // AdminMenu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 261);
            this.Controls.Add(this.AccountBtn);
            this.Controls.Add(this.PreviewBtn);
            this.Controls.Add(this.EditImgBtn);
            this.Name = "AdminMenu";
            this.Text = "Main Menu";
            this.ResumeLayout(false);
            this.Disposed += Shutdown;
        }

        #endregion

        private System.Windows.Forms.Button EditImgBtn;
        private System.Windows.Forms.Button PreviewBtn;
        private System.Windows.Forms.Button AccountBtn;
    }
}