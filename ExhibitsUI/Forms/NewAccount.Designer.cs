﻿namespace ExhibitsUI.Forms
{
    partial class NewAccount 
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.UserTxt = new System.Windows.Forms.TextBox();
            this.PassTxt = new System.Windows.Forms.TextBox();
            this.CreateBtn = new System.Windows.Forms.Button();
            this.userLbl = new System.Windows.Forms.Label();
            this.passLbl = new System.Windows.Forms.Label();
            this.confirmLbl = new System.Windows.Forms.Label();
            this.ConfirmTxt = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // UserTxt
            // 
            this.UserTxt.Location = new System.Drawing.Point(113, 29);
            this.UserTxt.Name = "UserTxt";
            this.UserTxt.Size = new System.Drawing.Size(352, 20);
            this.UserTxt.TabIndex = 0;
            // 
            // PassTxt
            // 
            this.PassTxt.Location = new System.Drawing.Point(113, 59);
            this.PassTxt.Name = "PassTxt";
            this.PassTxt.PasswordChar = '*';
            this.PassTxt.Size = new System.Drawing.Size(352, 20);
            this.PassTxt.TabIndex = 1;
            // 
            // ConfirmTxt
            // 
            this.ConfirmTxt.Location = new System.Drawing.Point(112, 87);
            this.ConfirmTxt.Name = "ConfirmTxt";
            this.ConfirmTxt.PasswordChar = '*';
            this.ConfirmTxt.Size = new System.Drawing.Size(352, 20);
            this.ConfirmTxt.TabIndex = 2;
            // 
            // CreateBtn
            // 
            this.CreateBtn.Location = new System.Drawing.Point(182, 132);
            this.CreateBtn.Name = "CreateBtn";
            this.CreateBtn.Size = new System.Drawing.Size(179, 23);
            this.CreateBtn.TabIndex = 3;
            this.CreateBtn.Text = "Create Account";
            this.CreateBtn.UseVisualStyleBackColor = true;
            this.CreateBtn.Click += new System.EventHandler(this.CreateAccount);
            // 
            // userLbl
            // 
            this.userLbl.AutoSize = true;
            this.userLbl.Location = new System.Drawing.Point(33, 33);
            this.userLbl.Name = "userLbl";
            this.userLbl.Size = new System.Drawing.Size(55, 13);
            this.userLbl.TabIndex = 4;
            this.userLbl.Text = "Username";
            // 
            // passLbl
            // 
            this.passLbl.AutoSize = true;
            this.passLbl.Location = new System.Drawing.Point(33, 64);
            this.passLbl.Name = "passLbl";
            this.passLbl.Size = new System.Drawing.Size(53, 13);
            this.passLbl.TabIndex = 5;
            this.passLbl.Text = "Password";
            // 
            // confirmLbl
            // 
            this.confirmLbl.AutoSize = true;
            this.confirmLbl.Location = new System.Drawing.Point(35, 92);
            this.confirmLbl.Name = "confirmLbl";
            this.confirmLbl.Size = new System.Drawing.Size(42, 13);
            this.confirmLbl.TabIndex = 6;
            this.confirmLbl.Text = "Confirm";
            // 
            // NewAccount
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(497, 184);
            this.Controls.Add(this.confirmLbl);
            this.Controls.Add(this.ConfirmTxt);
            this.Controls.Add(this.passLbl);
            this.Controls.Add(this.userLbl);
            this.Controls.Add(this.CreateBtn);
            this.Controls.Add(this.PassTxt);
            this.Controls.Add(this.UserTxt);
            this.Name = "NewAccount";
            this.Text = "New Account";
            this.ResumeLayout(false);
            this.PerformLayout();
        }

        #endregion

        private System.Windows.Forms.Button CreateBtn;
        private System.Windows.Forms.Label userLbl;
        private System.Windows.Forms.Label passLbl;
        private System.Windows.Forms.Label confirmLbl;
        private System.Windows.Forms.TextBox UserTxt;
        private System.Windows.Forms.TextBox PassTxt;
        private System.Windows.Forms.TextBox ConfirmTxt;
    }
}